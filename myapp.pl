#!/usr/bin/env perl
use v5.16;
use warnings;
use Mojolicious::Lite;
use MongoDB;
use JSON;
use experimental 'switch';

my $mongo = MongoDB->connect('mongodb://localhost');

# Documentation browser under "/perldoc"
plugin 'PODRenderer';

sub to_server {
    my $server = {};
    @{ $server }{qw(name id capacity)} = @{ $_ }{qw(name id capacity)};
    $server;
}

under sub {
    my $c = shift;
    $c->res->headers->access_control_allow_origin('*');
    1;
};

get '/' => sub {
    my $c = shift;
    $c->render(template => 'index');
};

sub handle_options {
    my $c = shift;
    $c->res->headers->access_control_allow_origin('*');
    $c->res->headers->header('Access-Control-Allow-Headers' => '*');
    $c->res->headers->header('Access-Control-Allow-Methods' => '*');
    $c->res->headers->header('Allow' => 'application/json');
    $c->res->headers->remove('Content-Type');
    $c->rendered(200);
}

options '/servers' => \&handle_options;
options( $_ => \&handle_options )
    for qw[/servers /servers/wat /appName];

get '/appName' => sub {
    my $c = shift;
    my $app_info = $mongo->ns('udemy.appInfo')->find_one({});
    $c->render(json => $app_info);
};

post '/servers' => sub {
    my $c = shift;
    my $servers = $c->req->json;
    say 'inserting data: ', JSON->new->canonical->encode($servers);
    $mongo->ns('udemy.servers')->insert_many($servers);
    $c->render(json => $servers);
};

put '/servers' => sub {
    my $c = shift;
    my $servers = $c->req->json;
    update_servers($servers);
    $c->render(json => $servers);
};

sub update_servers {
    my $servers = shift;
    my $coll = $mongo->ns('udemy.servers');
    my $cursor = $coll->find;

    my $i = 0;
    while (my $doc = $cursor->next) {
        
        last unless defined $servers->[$i];
        my $server = $servers->[$i];
        my ($name, $capacity, $id) = @{ $server }{qw(name capacity id)};
        unless ([ $name, $capacity, $id ] ~~ [ @{ $doc }{qw(name capacity id)} ]) {
            say 'updating data: ', JSON->new->canonical->encode($server);
            $coll->update_one(
                    { _id => $doc->{_id} },
                    { '$set' => { name => $name, capacity => $capacity, id => $id } }
            );
        }

        ++$i;
    }

    if (@$servers > 0 && $i < @$servers) {
        $coll->insert_many( [ @{ $servers }[$i .. $#$servers] ] );
    }

    undef;
}

get '/servers' => sub {
    my $c = shift;
    my @servers = map &to_server, $mongo->ns('udemy.servers')->find->all;
    say 'serving up servers: ', JSON->new->canonical->encode(\@servers);
    $c->render(json => \@servers);
};

app->start;
__DATA__

@@ index.html.ep
% layout 'default';
% title 'Welcome';
<h1>Welcome to the Mojolicious real-time web framework!</h1>
To learn more, you can browse through the documentation
<%= link_to 'here' => '/perldoc' %>.

@@ layouts/default.html.ep
<!DOCTYPE html>
<html>
  <head><title><%= title %></title></head>
  <body><%= content %></body>
</html>
